FROM maven:3.8.6-amazoncorretto-11 AS builder
WORKDIR /artemiscounter/
COPY artemis_counter ./
RUN mvn clean install

FROM artemisis
#RUN mkdir ./web

WORKDIR /var/lib/artemis-instance

COPY /artemis_config/bootstrap.xml ./etc/
COPY /artemis_config/broker.xml ./etc/
COPY /artemis_config/logging.properties ./etc/

COPY --from=builder /artemiscounter/artemis-metrics/target/artemis-prometheus-metrics-plugin-1.1.0.jar ./lib/
COPY --from=builder /artemiscounter/artemis-interceptor/target/artemis-interceptor-1.1.0.jar ./lib/
COPY --from=builder /artemiscounter/artemis-servlet/target/metrics.war ./web/

#CMD ["./bin/artemis run"]
