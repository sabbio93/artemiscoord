/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.activemq.artemis.core.server.metrics.plugins;

import java.util.Map;
import java.util.function.Predicate;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Meter.Id;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.prometheus.PrometheusConfig;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.apache.activemq.artemis.core.server.metrics.ActiveMQMetricsPlugin;

public class ArtemisPrometheusMetricsPlugin implements ActiveMQMetricsPlugin {

    MeterRegistry meterRegistry;

    @Override
    public ActiveMQMetricsPlugin init(Map<String, String> options) {
        // this.meterRegistry = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
        MeterRegistry reg = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT);
        Predicate<Id> pr = id -> {
            System.out.println(id.getTag("queue"));
            if (id.getTag("queue") != null && id.getName().compareTo("artemis.message.count") == 0) {
                return id.getTag("queue").contains("COUNT");
            }
            return false;
        };
        reg.config().meterFilter(MeterFilter.accept(pr)).meterFilter(MeterFilter.deny());
        this.meterRegistry = reg;
        return this;
    }

    @Override
    public MeterRegistry getRegistry() {
        return meterRegistry;
    }
}
