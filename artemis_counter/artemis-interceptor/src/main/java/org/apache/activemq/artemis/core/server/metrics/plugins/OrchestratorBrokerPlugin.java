/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.activemq.artemis.core.server.metrics.plugins;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.apache.activemq.artemis.api.core.*;

import org.apache.activemq.artemis.api.core.client.*;
import org.apache.activemq.artemis.core.message.impl.CoreMessage;
import org.apache.activemq.artemis.core.postoffice.RoutingStatus;
import org.apache.activemq.artemis.core.server.ServerSession;
import org.apache.activemq.artemis.core.server.plugin.ActiveMQServerPlugin;
import org.apache.activemq.artemis.core.transaction.Transaction;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.jboss.logging.Logger;
import org.msgpack.core.MessagePack;
import org.msgpack.core.MessageUnpacker;

public class OrchestratorBrokerPlugin implements ActiveMQServerPlugin {
    private final Map<String, Trigger> countingMap;
    private static final Logger logger = Logger.getLogger(OrchestratorBrokerPlugin.class);
    private ClientProducer clientProducer = null;
    private ClientSession clientSession = null;

    public OrchestratorBrokerPlugin() {
        super();
        logger.info(" =========== OrchestratorBrokerPlugin init =========== ");
        countingMap = new HashMap<>();
    }

    private void setupLocalClient() {
        if (clientSession == null || clientProducer == null) {
            try {
                ServerLocator locator = ActiveMQClient.createServerLocator("tcp://127.0.0.1:61616");
                ClientSessionFactory factory = locator.createSessionFactory();
                clientSession = factory.createSession();
                clientProducer = clientSession.createProducer();
            } catch (Exception e) {
                logger.error("Could not create ClientSession : " + e);
            }
        }
    }

    private void processTriggerMessage(Message message) {
        String targetQueue = null;
        String destinationQueue = null;
        int targetValue = 0;

        int x = message.toCore().getDataBuffer().readableBytes();
        byte[] arr = new byte[x];
        message.toCore().getDataBuffer().readBytes(arr);

        MessageUnpacker unpacker = MessagePack.newDefaultUnpacker(arr);
        System.out.println(Arrays.toString(arr));
        try {
            targetQueue = unpacker.unpackString();
            destinationQueue = unpacker.unpackString();
            targetValue = unpacker.unpackInt();
            unpacker.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        setupLocalClient();

        if (countingMap.containsKey(targetQueue)) {
            logger.info("Trigger already registered --- " + countingMap.get(targetQueue).toString());
        } else {
            Trigger trg = new Trigger(targetQueue, destinationQueue, targetValue);
            logger.info("New trigger --- " + trg);
            countingMap.put(targetQueue, trg);
        }
    }

    private void processSyncMessage(String queueAddress, Message message) throws ActiveMQException {
        if (countingMap.containsKey(queueAddress)) {
            Trigger trg = countingMap.get(queueAddress);
            if (trg.incrementAndCheck(1)) {
                logger.debug("Trigger resolved --- " + trg);
                ClientProducer producer = clientSession.createProducer(trg.getDestinationQueue());
                ClientMessage resolveMessage = clientSession.createMessage(false);
                resolveMessage.getBodyBuffer().writeString("Hello");
                producer.send(resolveMessage);
                countingMap.remove(queueAddress);
            }
            logger.info("Trigger update --- " + trg);
        } else {
            logger.info(queueAddress + " not in counting map!");
        }
    }

    @Override
    public void afterSend(ServerSession session,
                          Transaction tx,
                          Message message,
                          boolean direct,
                          boolean noAutoCreateQueue,
                          RoutingStatus result) throws ActiveMQException {
        logger.debug("--- afterSend call start ---");

        String queueAddress = message.getAddress();
        logger.debug(queueAddress);

        if (queueAddress.startsWith("__")) {
            if (queueAddress.startsWith("syn", 2)) {
                processSyncMessage(queueAddress, message);
            } else if (queueAddress.startsWith("trg", 2)) {
                processTriggerMessage(message);
            }
        }

        logger.debug("--- afterSend call end ---");
    }
}