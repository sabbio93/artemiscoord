package org.apache.activemq.artemis.core.server.metrics.plugins;

public class Trigger {
    private final int targetValue;
    private final String targetQueue;
    private final String destinationQueue;
    private int count = 0;

    public Trigger(String targetQueue, String destinationQueue, int targetValue) {
        this.targetValue = targetValue;
        this.targetQueue = targetQueue;
        this.destinationQueue = destinationQueue;
    }

    public Boolean incrementAndCheck(int value) {
        count += value;
        return count == targetValue;
    }

    public int getTargetValue() {
        return targetValue;
    }

    public String getTargetQueue() {
        return targetQueue;
    }

    public String getDestinationQueue() {
        return destinationQueue;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Trigger{" +
                "targetValue=" + targetValue +
                ", targetQueue='" + targetQueue + '\'' +
                ", destinationQueue='" + destinationQueue + '\'' +
                ", count=" + count +
                '}';
    }
}
